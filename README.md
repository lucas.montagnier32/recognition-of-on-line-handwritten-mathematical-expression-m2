# Project

Lucas MONTAGNIER, Adrien LESÉNÉCHAL

## Project Overview: Recognition of On-line Handwritten Mathematical Expression
#### Models:
Three models were used for this project:
- **Segmenter**: Groups strokes into symbols.
- **Classifier**: Recognizes symbols and assigns them a class.
- **Analyzer**: Establishes relationships between symbols.

The architectures of the models, training functions, and their evaluations are detailed in the sections of this notebook. The three trained models are in the models folder.

**Dataset Creation:** 9 datasets have been created for train, validation, and test sets for each task from the same inkml files in the data folder.

#### General execution flow (main function):
1. **Segmentation:** Application of the segmenter on strokes, resulting in an LG file with grouped strokes.
2. **Classification:** Application of the classifier on symbols, producing an LG file with recognized symbols.
3. **Hypothesis Selection:** Application of an algorithm to select the best hypotheses for symbols.
4. **Symbolic Relationship Analysis:** Application of the analyzer to create relationships between the recognized symbols.
5. **Equation Tree Selection:** Application of an algorithm to select the best tree of the found equation.
